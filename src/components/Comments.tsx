import { Article } from "@/entities";

interface Props {
  article: Article;
}

export default function Comments({ article }: Props) {
  return (
    <>
      
        <h3 className="d-flex text-left p-0">Commentaires</h3>
     
      <div className="list-group mt-4 mb-4">
        {article.comments?.map((comment) => (
          <div
            key={comment.id}
            className="list-group-item row d-flex justify-content-between"
          >
            <div className="col-md-8 commLeft">
              <p className="mb-1">"{comment.content}"</p>
            </div>
            <div className="col-md-4 commRight">
              <small>
                {comment.author} {comment.date?.slice(0, 10)}{" "}
              </small>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
