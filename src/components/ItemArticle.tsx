import { Article, Image } from "@/entities";
import Link from "next/link";
import FavoriteIcon from "@mui/icons-material/Favorite";
import VisibilityIcon from "@mui/icons-material/Visibility";

interface Props {
  article: Article;
}

export default function ItemArticle({ article }: Props) {
  const categories = article.categories
    ?.map((category) => category.name)
    .join(", ");

  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  return (
    <div className="mt-2">
      {article.images?.length ? (
        <img
          className="card-img-top"
          key={article.id}
          src={makeLink(String(article.images[0].link))}
          alt="image de l'article"
          style={{ height: 28 + "rem" }}
        />
      ) : (
        <img
          src="https://www.ehess.fr/sites/default/files/styles/taille_image_contenu__870/public/evenements/images/design-.jpg?itok=Cxl041fI"
          alt="image de l'article"
          style={{ height: 20 + "rem", width: 30 + "rem" }}
        />
      )}
      <div className="card-body">
        <h5 className="card-title mt-4">{article.title}</h5>
        <p
          className="text-center"
          style={{ fontSize: 14 + "px", marginTop: 10 + "px" }}
        >
          {categories}
        </p>
        <p className="card-text">
          {article.content?.split(" ").slice(0, 34).join(" ")}...
        </p>
        <div className="row my-4">
          <div className="col-6">
            {" "}
            <VisibilityIcon /> {article.views}
          </div>
          <div className="col-6">
            {" "}
            <FavoriteIcon style={{ color: "red" }} /> {article.likes?.length}
          </div>
        </div>
        <Link href={"/article/" + article.id} className="btn btn-bleu btn-sm mb-4">
          Lire l'article
        </Link>
      </div>
    </div>
  );
}
