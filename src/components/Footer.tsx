
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import EmailIcon from '@mui/icons-material/Email';
import Link from 'next/link';

export default function Footer() {
    
    return(
        <div className="row footer d-flex justify-content-between pt-4 pb-1">
            <div className=" col-md-4 footerLogo ps-4">
        <img src="/stairslogo.png" alt="Logo" width="50" height="44" className="d-inline-block align-text-center"/> Art&Design
        </div>
        <div className="col-md-4 text-center">
        <p className="mt-2 ">Confidentialité / Politique relative aux cookies / Mentions légales / Accessibilité</p>
        </div>
        <div className="col-md-4 footerRS pe-4">
        <Link className="m-2" href={`mailto: archi@gmail.com`}><EmailIcon/></Link>
        <Link className="m-2" href={"https://facebook.com"}><FacebookIcon/></Link>
        <Link className="m-2" href={"https://instagram.com"}><InstagramIcon/></Link>
        </div>
        </div>
        )
        
    }