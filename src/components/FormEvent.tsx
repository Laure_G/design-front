import { Event } from "@/entities";
import { PhotoCamera } from "@mui/icons-material";
import { Button } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";

interface Props {
  onSubmit: (event: Event) => void;
  edited?: Event;
}

export default function FormEvent({ onSubmit, edited }: Props) {
  const router = useRouter();

  const [errors, setErrors] = useState("");

  const [evenement, setEvenement] = useState<Event>(
    edited
      ? edited
      : {
          title: "",
          date: "",
          adresse: "",
          content: "",
          image: "",
        }
  );

  function handleChange(event: any) {
    setEvenement({
      ...evenement,
      [event.target.name]: event.target.value,
    });
  }

  function handleFile(event: any) {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      setEvenement({
        ...evenement,
        image: fileReader.result as string,
      });
    };
    fileReader.readAsDataURL(event.target.files[0]);
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();
    try {
      onSubmit(evenement);
    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }
    }
  }

  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  return (
    <div className="row d-flex justify-content-center m-4">
      <div className="col-sm-7 col-md-8 formulaire" style={{textAlign:"initial"}}>
        <form onSubmit={handleSubmit} className="mt-3 mb-4">
          {errors && <p>{errors}</p>}
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Titre de l'event:
            </label>
            <input
              type="text"
              name="title"
              className="form-control"
              value={evenement.title}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="adresse" className="form-label">
              Adresse:
            </label>
            <input
              type="text"
              name="adresse"
              className="form-control"
              value={evenement.adresse}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="date" className="form-label">
              Date:
            </label>
            <input
              type="date"
              name="date"
              className="form-control"
              value={evenement.date}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="content" className="form-label">
              Rédaction du corps de l'article:
            </label>
            <textarea
              name="content"
              className="form-control"
              value={evenement.content}
              onChange={handleChange}
              required
            />
            <div>
              <Button
                color="info"
                variant="contained"
                component="label"
                className="btn btn-bleu mt-3 mb-3"
                endIcon={<PhotoCamera />}
              >
                Ajouter une image{" "}
                <input type="file" hidden name="src" onChange={handleFile} />
              </Button>
              </div>
              {evenement.image && (
                <img src={evenement.image} style={{ height: 100 }} />
              )}
            
          </div>

          <button type="submit" className="btn btn-bleu">
            Ajouter
          </button>
        </form>
      </div>
    </div>
  );
}
