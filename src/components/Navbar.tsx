import Link from "next/link";
import { AccountCircle } from "@mui/icons-material";
import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";
import jwtDecode from "jwt-decode";

export default function Navbar() {
  const { token } = useContext(AuthContext);

  function isLog() {
    if (token) {
      window.location.href = "/account";
    } else {
      window.location.href = "/login";
    }
  }

  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  return (
    <nav className="navbar navbar-expand-lg px-2">
      <div className="container-fluid">
        <Link className="navbar-brand" href={"/"}>
          <img src="/stairslogo.png" alt="Logo" width="50" height="44" className="d-inline-block align-text-center"/>{" "}
          Art&Design
        </Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {isAdmin() && (
              <li className="nav-item">
                <Link className="nav-link " aria-current="page" href={"/article/add/"} >
                  Ajouter un article
                </Link>
              </li>
            )}
            {isAdmin() && (
              <li className="nav-item">
                <Link className="nav-link " aria-current="page" href={"/event/add/"}>
                  Ajouter un évènement
                </Link>
              </li>
            )}
            <li>
              <Link onClick={isLog} className="nav-link " aria-current="page" href={"#"}>
              <AccountCircle /> 
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
