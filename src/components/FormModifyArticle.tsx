import { Article, Image } from "@/entities";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";

interface Props {
  edited?: Article;
  onSubmit: (article: Article) => void;
}

export default function FormModifyArticle({ onSubmit, edited }: Props) {
  const router = useRouter();
  const [errors, setErrors] = useState("");

  const [article, setArticle] = useState<Article>(
    edited
      ? edited
      : {
          title: "",
          author: "",
          content: "",
        }
  );

  function handleChange(event: any) {
    setArticle({
      ...article,
      [event.target.name]: event.target.value,
    });
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();
    try {
      onSubmit(article);
    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }
    }
  }

  return (
    <div className="row d-flex justify-content-center m-4">
      <div
        className="col-sm-7 col-md-8 formulaire "
        style={{ textAlign: "initial" }}
      >
        <form onSubmit={handleSubmit} className="mt-3 mb-4">
          {errors && <p>{errors}</p>}
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Titre de l'article:
            </label>
            <input
              type="text"
              name="title"
              className="form-control"
              value={article.title}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="author" className="form-label">
              Nom de l'auteur:
            </label>
            <input
              type="text"
              name="author"
              className="form-control"
              value={article.author}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="content" className="form-label">
              Rédaction du corps de l'article:
            </label>
            <textarea
              name="content"
              className="form-control"
              value={article.content}
              onChange={handleChange}
              required
            />
          </div>

          <button
            onClick={() => location.reload()}
            className="btn btn-bleu mt-3 me-3"
          >
            Annuler
          </button>
          <button type="submit" className="btn btn-bleu mt-3 ms-3">
            Ajouter
          </button>
        </form>
      </div>
    </div>
  );
}
