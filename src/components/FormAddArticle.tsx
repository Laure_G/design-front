import { postArticle } from "@/article-service";
import { Article, Image } from "@/entities";
import { postImage } from "@/image-service";
import { PhotoCamera } from "@mui/icons-material";
import { Button } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";


interface Props {
    onSubmit: any
}

export default function FormAddArticle({ onSubmit}:Props) {
    
    const router = useRouter();
    
    const [errors, setErrors] = useState('');
    const [image, setImage] = useState<Image>();

    
    const [article, setArticle] = useState<Article>({
        title: '',
        author: '',
        content: '',
    });
  
    function handleFile(event: any) {
//Pour le multi image : changer le type du useState en Image[], puis ici dans cette fonction
//Faire une boucle sur event.target.files et dans cette boucle mettre tout ce qu'il y a en dessous et dans le setImage on met un [...image, {link: reader.result as string}]
//Dans le handleSubmit, il faudra mettre le postImage dans une boucle sur image également

        const reader = new FileReader();
        reader.onload = () => {
            setImage({link: reader.result as string})
        };

        reader.readAsDataURL(event.target.files[0]);        
    }
    
    
    function handleChange(event: any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }
    
    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
        const posted = await postArticle (article);
        await postImage({...image, idArticle:posted.id})
        onSubmit(posted)
        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }
    
    return (
        <div className="row d-flex justify-content-center m-4">
        <div className="col-sm-7 col-md-8" style={{boxShadow:"3px 3px 15px #A2B4F0", backgroundColor:"white", borderRadius:"10px"}}>
        
        <form onSubmit={handleSubmit} className="mt-3 mb-4">
        {errors && <p>{errors}</p>}
        <div className="mb-3">
        <label htmlFor="title" className="form-label">Titre de l'article</label>
        <input type="text" name="title" className="form-control" value={article.title} onChange={handleChange} required/>
        </div>
        <div className="mb-3">
        <label htmlFor="author"className="form-label">Nom de l'auteur</label>
        <input type="text" name="author" className="form-control" value={article.author} onChange={handleChange} required/>
        </div>
        <div className="mb-3">
        <label htmlFor="content" className="form-label">Rédaction du corps de l'article</label>
        <textarea name="content" className="form-control" value={article.content} onChange={handleChange} required/>
    
        </div >
        <div className="mb-3">
     
                        <Button color="info" variant="contained" component="label" className="btn btn-bleu mt-3 mb-3" endIcon={ <PhotoCamera />} >
                            Ajouter une image <input
                                type="file"
                                hidden
                                name="src"
                                onChange={handleFile}  /></Button>
                                 {image &&
                                   <img src={image.link} style={{ height: 100 }} />
                                  }
                    </div>
        <button type="submit" className="btn btn-bleu">Ajouter</button>
        </form>
        
        
        </div>
        </div>
        
        
        
        )
    }
    