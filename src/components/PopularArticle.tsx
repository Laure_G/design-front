import { Article } from "@/entities";
import Link from "next/link";

interface Props {
    article: Article;
}

export default function ItemArticle({article}: Props) {
   
    function makeLink(link:string) {
    
        if(link.startsWith('http')) {
          return link;
        } 
        return process.env.NEXT_PUBLIC_SERVER_URL+'/uploads/'+link;
      }

    return(
       
     <div className="row">
        <Link href={"/article/"+article.id} style={{textDecoration:"none", display:"flex", color: "black"}}>
        <div className="col-7">
        {article.images?.length ?
            <img className="card-img-top mb-2" key={article.id} src={makeLink(String(article.images[0].link))} alt="image de l'article" style={{height: 8+"rem"}} /> 
            : <img src="https://www.ehess.fr/sites/default/files/styles/taille_image_contenu__870/public/evenements/images/design-.jpg?itok=Cxl041fI" alt="image de l'article" style={{height: 8+"rem"}}/>
        }
           
        </div>
        <div className="col-4 ms-3">
        <h5 className="populartitre">{article.title}</h5>
        </div>
        </Link>
    </div>
       
        );
    }