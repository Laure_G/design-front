import { Comment } from "@/entities";
import { Button } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";


interface Props {
    onSubmit:(comment:Comment) => void;
}


export default function FormComment({onSubmit}:Props) {
    
    const router = useRouter();
    
    const [errors, setErrors] = useState('');
    
    const [comment, setComment] = useState<Comment>({
        'content': "",
    });
   
    
    function handleChange(event: any) {
        setComment({
            ...comment,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(comment);
            
        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }
    
    return (
        <div className="row d-flex justify-content-center">
        <div className="col-sm-7 col-md-8 formulaire ">
        
        <form onSubmit={handleSubmit} className="mt-3 mb-4">
        {errors && <p>{errors}</p>}
        <div className="mt-3 mb-3">
        <label htmlFor="content" className="form-label">Votre commentaire:</label>
        <textarea name="content" className="form-control" value={comment.content} onChange={handleChange} required/>
    
        </div>
                    <Button color="info" variant="contained" type="submit" className="btn btn-primary mt-4 mb-4">Valider</Button>
        
        </form>
        
        </div>
        </div>

        
        )
    }
    