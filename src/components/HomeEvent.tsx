import { Event } from "@/entities";
import Link from "next/link";

interface Props {
  event: Event;
}

export default function HomeEvent({ event }: Props) {
  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  return (
    <div className="row">
      <Link href={"/event/" + event.id} style={{textDecoration:"none", display:"flex", color: "black"}}>
        <div className="col-7">
          <img
            src={makeLink(String(event.image))}
            className="card-img-top mb-2"
            alt="..."
            style={{ height: 8 + "rem" }}
          />
        </div>
        <div className="col-4 ms-3">
          <h5 className="populartitre">{event.title}</h5>
        </div>
      </Link>
    </div>
  );
}
