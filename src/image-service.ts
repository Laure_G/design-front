import axios from "axios";
import { Image } from "./entities";

export async function fetchAllImages(){
    const response= await axios.get <Image[]> ( '/api/image');
    return response.data;
}

export async function fetchOneImage(id:number){
    const response= await axios.get <Image> ('/api/image/'+id);
    return response.data;
}

export async function updateImage(image:Image) {
    const response = await axios.put<Image>('/api/image/'+image.id, image);
    return response.data;
}

export async function postImage(image:Image) {
    const response = await axios.post<Image>('/api/image', image);
    return response.data;
}

export async function deleteImage(id:any) {
    await axios.delete('/api/image/'+id);
}
