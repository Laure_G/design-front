import axios from "axios";
import { Event } from "./entities";

export async function fetchAllEvents(){
    const response= await axios.get <Event[]> ( '/api/event');
    return response.data;
}

export async function fetchOneEvent(id:number){
    const response= await axios.get <Event> ('/api/event/'+id);
    return response.data;
}

export async function updateEvent(event:Event) {
    const response = await axios.put<Event>('/api/event/'+event.id, event);
    return response.data;
}

export async function postEvent(event:Event) {
    const response = await axios.post<Event>('/api/event', event);
    return response.data;
}

export async function deleteEvent(id:any) {
    await axios.delete('/api/event/'+id);
}

export async function addSubscription(id:number|string) {
    const response = await axios.post(`/api/event/${id}/subscribe`);
    return response.data;
}

export async function deleteSubscription(id:number|string) {
    const response = await axios.delete(`/api/event/${id}/unsubscribe`);
    return response.data;
}

export async function isSubscribed(id:number|string) {
    const response = await axios.get<boolean>(`/api/event/${id}/subscribe`);
    return response.data;
}