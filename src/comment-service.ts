import axios from "axios";
import { Comment, Article } from "./entities";

export async function postComment(comment:Comment, article:Article) {
    const response = await axios.post<Comment>('/api/comment/'+ article.id, comment);
    return response.data;
}

