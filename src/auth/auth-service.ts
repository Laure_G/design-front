import { User } from "@/entities";
import axios from "axios";

export async function login(email: string, password: string) {
  const response = await axios.post<{ token: string }>("/api/login", {
    email,
    password,
  });
  return response.data.token;
}

export async function fetchUser() {
  const response = await axios.get<User>("/api/account");
  return response.data;
}

export async function UserArticleLikes() {
  const response = await axios.get("api/protected/likes");
  return response.data;
}

export async function UserEvents() {
  const response = await axios.get("api/protected/events");
  return response.data;
}

export async function deleteUser(id: any) {
  await axios.delete("/api/user/" + id);
}