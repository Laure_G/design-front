import axios from "axios";
import { Article } from "./entities";

export async function fetchAllArticles() {
  const response = await axios.get<Article[]>("/api/article");
  return response.data;
}

export async function fetchAllArticlesByPopularity() {
  const response = await axios.get<Article[]>("/api/article/popular");
  return response.data;
}

export async function fetchOneArticle(id: number) {
  const response = await axios.get<Article>("/api/article/" + id);
  return response.data;
}

export async function updateArticle(article: Article) {
  const response = await axios.put<Article>("/api/article/" + article.id, article);
  return response.data;
}

export async function postArticle(article: Article) {
  const response = await axios.post<Article>("/api/article", article);
  return response.data;
}

export async function deleteArticle(id: any) {
  await axios.delete("/api/article/" + id);
}

export async function likeArticle(id: number | string) {
  const response = await axios.post(`/api/article/${id}/like`);
  return response.data;
}
