export interface Article {
    id? : number;
    title?:string;
    content?: string;
    author?: string;
    date?: string;
    views?: number;
    likes?: number[];
    userLiked?: boolean;
    images?: Image[];
    categories?: Category[];
    comments?: Comment[];
}


export interface Image {
    id?: number;
    link?: string;
    idArticle?:number;
}

export interface Category {
    id?: number;
    name?: string;
    articles?: Article[];
}

export interface Comment {
    id?: number;
    date?: string;
    content?: string;
    author?: string;
}

export interface User {
    id?: number;
    name?: string;
    lastname?: string;
    email: string;
    password: string;
    role?: string
}

export interface Event {
    id?: number;
    title?: string;
    date?: string;
    adresse?: string;
    content?: string;
    image?: string;
    isSubscribe?: boolean;
}