import {
  fetchAllArticles,
  fetchAllArticlesByPopularity,
} from "@/article-service";
import HomeEvent from "@/components/HomeEvent";
import ItemArticle from "@/components/ItemArticle";
import PopularArticle from "@/components/PopularArticle";
import { Article, Event } from "@/entities";
import { fetchAllEvents } from "@/event-service";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import React from "react";
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

interface Props {
  articles: Article[];
  popularArticles: Article[];
  events: Event[];
}

export default function Index({ articles, popularArticles, events }: Props) {
  const firstSixArticles = articles.slice(0, 6);
  const firstFiveArticles = popularArticles.slice(0, 5);
  const firstFiveEvents = events.slice(0, 5);
  const router = useRouter();

  return (
    <div>
      <div className="col justify-content-center header mb-5">
        <div className="title mb-2 pb-0">
          {/* <div className="line3"></div>  */}
          <h1 className="text-left mb-2">
            <span style={{ lineHeight: 120 + "%" }}>Art,</span>
            <br></br>
            <span style={{ fontSize: 90 + "%" }}>
              Design{" "}
              <span
                style={{
                  WebkitTextStroke: "2px white",
                  color: "rgba(255, 255, 255, 0)",
                }}
              >
                &
              </span>
            </span>{" "}
            <br></br>
            <span style={{ fontSize: 70 + "%" }}>Architecture</span>{" "}
          </h1>

          <h3 className="text-center mt-1 p-0">
            Les plus belles oeuvres visuelles à travers le monde
          </h3>
        </div>
        <div className="line2"></div>
      </div>

      <div className="row d-flex justify-content-around ">
        <div className="col-sm-10 col-md-9">
          <div className="row d-flex justify-content-between px-4">
            <h2>Articles récents</h2>
            {firstSixArticles.map((item) => (
              <div className="col-md-6 text-center mb-4" key={item.id}>
                <ItemArticle article={item} />{" "}
              </div>
            ))}
            <div className=" text-center ">
              <button
                type="button"
                className="btn btn-bleu m-5"
                onClick={() => router.push("/article/all")}>
                Découvrir tous nos articles  <ArrowForwardIosIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="articlepop">
            <h2>Articles populaires</h2>
            {firstFiveArticles.map((popular) => (
              <div className="col mt-3" key={popular.id}>
                <PopularArticle article={popular} />{" "}
              </div>
            ))}
          </div>

          <div className="homeEvent">
            <h2>Évènements</h2>
            {firstFiveEvents.map((event) => (
              <div className="col mt-3" key={event.id}>
                <HomeEvent event={event} />{" "}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      articles: await fetchAllArticles(),
      events: await fetchAllEvents(),
      popularArticles: await fetchAllArticlesByPopularity(),
    },
  };
};
