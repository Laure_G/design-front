import { Article, User, Event } from "@/entities";
import { useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import {
  UserArticleLikes,
  UserEvents,
  deleteUser,
  fetchUser,
} from "@/auth/auth-service";
import { AuthContext } from "@/auth/auth-context";
import Link from "next/link";

export default function UserPage() {
  const [user, setUser] = useState<User>();
  const [likedArticle, setlikedArticle] = useState<Article[]>();
  const [userEvents, setuserEvents] = useState<Event[]>();
  const { setToken } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    fetchUser().then((data) => {
      setUser(data);
    });
  }, []);

  useEffect(() => {
    UserArticleLikes().then((datas) => {
      setlikedArticle(datas);
    });
  }, []);

  useEffect(() => {
    UserEvents().then((infos) => {
      setuserEvents(infos);
    });
  }, []);

  async function logout() {
    setToken(null);
    router.push("/#");
  }

  async function deleteAccount() {
    await deleteUser(user?.id);
    setToken(null);
    router.push("/#");
  }

  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  return (
    <>
      <div className="container-fluid py-2">
        <div className="row mt-4 m-2">
          <div className="col-md-6">
            <h1 style={{ justifyContent: "left", padding: 0 + "px" }}>
              {user?.name} {user?.lastname}
            </h1>
            <p className="">E-mail: {user?.email} </p>
          </div>
          <div className="col-md-3 deconnexion">
            <button
              className="btn btn-bleu mt-3 mb-3"
              onClick={() => deleteAccount()}
            >
              Supprimer son compte
            </button>
          </div>
          <div className="col-md-3 deconnexion">
            <button className="btn btn-bleu mt-3 mb-3" onClick={() => logout()}>
              Se déconnecter
            </button>
          </div>
        </div>
      </div>
      <div className="row m-2">
        <h2>Mes favoris</h2>
        {likedArticle?.map((item) => (
          <div className="col-md-3 card-compte text-center mt-3" key={item.id}>
            <Link
              href={"/article/" + item.id}
              style={{ textDecoration: "none", color: "black" }}
            >
              <h5>{item.title}</h5>
              {item.images && (
                <img
                  src={makeLink(String(item.images[0].link))}
                  alt={item.author}
                  className="card-img-top "
                  style={{ height: 20 + "rem" }}
                />
              )}
              <div className="card-compte-text">
                <h6 className="pt-2 mb-1">{item.author}</h6>
                <p>{item.date?.slice(0, 10)}</p>
              </div>{" "}
            </Link>
          </div>
        ))}
      </div>

      <div className="row my-5 mx-2">
        <h2>Mes évènements</h2>
        {userEvents?.map((userEvent) => (
          <div
            className="col-md-3 card-compte text-center mt-3"
            key={userEvent.id}
          >
            <Link
              href={"/event/" + userEvent.id}
              style={{ textDecoration: "none", color: "black" }}
            >
              <h5>{userEvent.title}</h5>
              <img
                src={makeLink(String(userEvent.image))}
                alt={userEvent.title}
                className="card-img-top "
                style={{ height: 20 + "rem" }}
              />
              <div className="card-compte-text">
                <h6 className="pt-2 mb-1">{userEvent.adresse}</h6>
                <p>{userEvent.date?.slice(0, 10)}</p>
              </div>
            </Link>
          </div>
        ))}
      </div>
    </>
  );
}
