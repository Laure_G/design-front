import FormAddArticle from "@/components/FormAddArticle";
import { Article } from "@/entities";
import { useRouter } from "next/router";

export default function Add() {
    const router = useRouter();

    async function addArticle(article: Article) {
        router.push('/article/' + article.id);
    }

    return(
       
        <div className="formulaireAjout">
            <h1 className="formulaireTitre text-center">Ajouter un nouvel article</h1>
            <FormAddArticle onSubmit={addArticle} />
        </div>
        
    )
}