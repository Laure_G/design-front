import {
  deleteArticle,
  fetchOneArticle,
  likeArticle,
  updateArticle,
} from "@/article-service";
import Comments from "@/components/Comments";
import FormModifyArticle from "@/components/FormModifyArticle";
import { Article, Comment } from "@/entities";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import { AuthContext } from "@/auth/auth-context";
import FormComment from "@/components/FormComment";
import { postComment } from "@/comment-service";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import VisibilityIcon from "@mui/icons-material/Visibility";

export default function ArticlePage() {
  const router = useRouter();
  const { id } = router.query;
  const [article, setArticle] = useState<Article>();
  const [showEdit, setShowEdit] = useState(false);
  const [addComment, setAddComment] = useState(false);
  const { token } = useContext(AuthContext);

  function getThisArticle() {
    fetchOneArticle(Number(id))
      .then((data) => setArticle(data))
      .catch((error) => {
        if (error.reponse.status == 404) {
          router.push("/404");
        }
      });
  }

  useEffect(() => {
    if (!id) {
      return;
    }
    getThisArticle();
  }, [id]);

  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  async function remove() {
    await deleteArticle(id);
    router.push("/");
  }
  async function update(article: Article) {
    const updated = await updateArticle(article);
    setArticle(updated);
  }

  async function saveComment(comment: Comment) {
    if (!article) return;
    const added = await postComment(comment, article);
    setArticle({ ...article, comments: [...article.comments!, added] });
  }

  function toggle() {
    setShowEdit(!showEdit);
  }
  function addNewComment() {
    setAddComment(!addComment);
  }

  async function like() {
    await likeArticle(Number(article?.id));
    getThisArticle();
  }

  if (!article) {
    return <p>Loading...</p>;
  }

  return (
    <div className="text-center">
      <h1 className="mb-2 pb-0">{article?.title}</h1>
      <div className="containerArticle row d-flex justify-content-center ">
        <p className="mb-4">
          {article?.author} -{" "}
          {article?.date && new Date(article?.date).toLocaleDateString()}
        </p>
        {article.images?.length ? (
          <img
            className="card-img-top mb-4"
            key={article.id}
            src={makeLink(String(article.images[0].link))}
            alt="image de l'article"
            style={{
              height: 50 + "vw",
              objectFit: "cover",
              objectPosition: "center",
            }}
          />
        ) : (
          <img
            src="https://www.ehess.fr/sites/default/files/styles/taille_image_contenu__870/public/evenements/images/design-.jpg?itok=Cxl041fI"
            alt="image de l'article"
            style={{ width: 80 + "vw" }}
          />
        )}
        <p className="m-4"> {article?.content} </p>

        <div className="row m-4">
          <div className="col-6" style={{display:"flex",alignItems:"center", justifyContent:"center"}}>
           
            <VisibilityIcon style={{ color: "blue" }} /> {article.views}
          </div>

          {token ? (
            <div className="col-6">
              <button
                className="btn btn-sm pe-3 ps-3 mt-4 me-2 mb-4"
                onClick={like}
              >
                {article.userLiked ? (
                  <FavoriteIcon style={{ color: "red" }} />
                ) : (
                  <FavoriteBorderIcon style={{ color: "red" }} />
                )}{" "}
                {article.likes?.length}
              </button>
            </div>
          ) : (
            <div className="col-6">
              {" "}
              <FavoriteIcon style={{ color: "red" }} /> {article.likes?.length}
            </div>
          )}
        </div>

        <div
          id="carouselExampleControls"
          className="carousel slide mt-5"
          data-bs-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              {article.images?.length ? (
                <img
                  className="card-img-top"
                  src={makeLink(String(article.images[0].link))}
                  alt={article.title}
                  style={{
                    height: 80 + "vh",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                />
              ) : (
                <img
                  src="https://images.unsplash.com/photo-1463154545680-d59320fd685d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2570&q=80"
                  alt="img-product"
                />
              )}
            </div>
            {article.images?.slice(1, 4).map((image) => (
              <div key={image.id} className="carousel-item">
                <img
                  src={makeLink(String(image.link))}
                  className="d-block w-100"
                  alt={article.title}
                  style={{
                    height: 80 + "vh",
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                />
              </div>
            ))}
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>

        <div className="row d-flex" style= {{marginTop:"50px"}}>
          <Comments article={article} />
          {token && (
            <div>
              <button
                className="btn btn-bleu btn-sm pe-3 ps-3 mt-4 me-2 mb-4"
                onClick={addNewComment}
              >
                Ajouter un commentaire
              </button>
            </div>
          )}

          {addComment && (
            <>
              <h2 className="mt-4">Ajouter un commentaire</h2>
              <FormComment onSubmit={saveComment} />
            </>
          )}
        </div>
        
        {showEdit && (
          <>
            <h2 style= {{marginTop:"60px", fontFamily:"Bentham"}}>Modifier l'article</h2>
            <FormModifyArticle edited={article} onSubmit={update} />
          </>
        )}

        {isAdmin() && (
          <div className="justify-content-between" style= {{display:"flex", marginBottom: "40px"}}>
            <button
              className="btn btn-bleu btn-sm pe-3 ps-3 mt-4 me-2 mb-4"
              onClick={toggle}
            >
                Modifier l'article
            </button>
            <button
              className="btn btn-bleu btn-sm pe-3 ps-3 mt-4 ms-2 mb-4"
              onClick={remove}
            >
              Supprimer l'article
            </button>
          </div>
        )}

  
      </div>
    </div>
  );
}
