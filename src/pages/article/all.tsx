import { fetchAllArticles } from "@/article-service";
import ItemArticle from "@/components/ItemArticle";
import { Article } from "@/entities";
import { fetchAllEvents } from "@/event-service";
import { GetServerSideProps } from "next";
import React from 'react'


interface Props {
    articles: Article[];
  }

export default function AllArticle({ articles }: Props) {

    return(
    
            <div>
               
                <div className="row d-flex justify-content-around ">
                    <div className="col">
                        <div className="row d-flex justify-content-around mt-4">
                             <h1>Art, Design & Architecture</h1>
                            {articles.map((item) =>
                             <div className="col-sm-10 col-md-5 text-center m-4" key= {item.id} >
                            <ItemArticle article ={item}/> </div>
                                )}
                        </div>
                    </div>
                  
                </div>
                        
            </div>
        )
    

}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
    return {
      props: {
        articles: await fetchAllArticles(),
        events: await fetchAllEvents(),
      },
    };
  };