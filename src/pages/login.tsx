import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { User } from "@/entities";
import { Button, Card, Form, Input } from "antd";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { red } from "@ant-design/colors";
import { register } from "@/register-service";


export default function loginPage() {
  const router = useRouter();
  const { setToken } = useContext(AuthContext);
  const [error, setError] = useState("");

  async function handleSubmit(values: User) {
    setError("");
    try {
      setToken(await login(values.email, values.password));
      router.push("/account/");
    } catch (error: any) {
      if (error.response?.status == 401) {
        setError("Invalid login/password");
      } else {
        setError('Erreur serveur');
      }
    }
  }

  async function handleSubmitRegister(values: User) {
    setError('');
    try {
        await register(values);

  setToken(await login(values.email, values.password));
  router.push("/account/");
} catch (error: any) {
  if (error.response?.status == 401) {
    setError("Invalid login/password");
  } else {
    setError('Erreur serveur');
  }
}
}

  return (
    <div className="row loginback d-flex px-3 align-content-center">
      <div className="col-md-6" >
      <Card style={{  margin: "50px 30px 50px 30px", boxShadow:"3px 3px 15px #A2B4F0"}}>
        <h2 className="loginTitle">Se connecter</h2>
        {error && <p style={{ color: red.primary }}>{error}</p>}
        <Form onFinish={handleSubmit}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item style={{marginBottom:"8px"}}>
            <Button type="primary" htmlType="submit" style={{backgroundColor:"#A2B4F0"}}>
              Se connecter
            </Button>
          </Form.Item>

        </Form>
      </Card>
      </div>
     <div className="col-md-6">
     <Card style={{ margin: "50px 30px 50px 30px", boxShadow:"3px 3px 15px #A2B4F0" }}>
        <h2 className="loginTitle">Créer un compte</h2>
        <Form onFinish={handleSubmitRegister}>
          <Form.Item
            label="Prénom"
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item
            label="Nom"
            name="lastname"
            rules={[{ required: true, message: "Please input your lastName!" }]}
          >
            <Input type="name" />
          </Form.Item>


          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: "Please input your email!" }]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Mot de passe"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item style={{marginBottom:"8px"}}>
            <Button type="primary" htmlType="submit" style={{backgroundColor:"#A2B4F0"}}>
              S'enregistrer
            </Button>
          </Form.Item>
        </Form>
      </Card>
     </div>
     
    </div>
  );
}
