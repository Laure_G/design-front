import FormEvent from "@/components/FormEvent";
import { Event } from "@/entities";
import { postEvent } from "@/event-service";
import { useRouter } from "next/router";

export default function Add() {
  const router = useRouter();

  async function addEvent(evenement: Event) {
    const added = await postEvent(evenement);
    router.push("/event/" + added.id);
  }

  return (
    <div className="formulaireAjout">
      <h1 className="formulaireTitre text-center">
        Ajouter un nouvel évènement
      </h1>
      <FormEvent onSubmit={addEvent} />
    </div>
  );
}
