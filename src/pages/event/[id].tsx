import { AuthContext } from "@/auth/auth-context";
import FormEvent from "@/components/FormEvent";
import { Event } from "@/entities";
import {
  addSubscription,
  deleteEvent,
  deleteSubscription,
  fetchOneEvent,
  isSubscribed,
  updateEvent,
} from "@/event-service";
import jwtDecode from "jwt-decode";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

export default function EventPage() {
  const router = useRouter();
  const { id } = router.query;
  const [event, setEvent] = useState<Event>();
  const [showEdit, setShowEdit] = useState(false);
  const [isSubscribe, setIsSubscribe] = useState<boolean>(false);
  const { token } = useContext(AuthContext);

  useEffect(() => {
    if (!id) {
      return;
    }
    fetchOneEvent(Number(id))
      .then((data) => {
        setEvent(data);
        isSub();
      })
      .catch((error) => {
        if (error.reponse.status == 404) {
          router.push("/404");
        }
      });
  }, [id]);

  function makeLink(link: string) {
    if (link.startsWith("http")) {
      return link;
    }
    return process.env.NEXT_PUBLIC_SERVER_URL + "/uploads/" + link;
  }

  async function remove() {
    await deleteEvent(id);
    router.push("/");
  }
  async function update(event: Event) {
    const updated = await updateEvent(event);
    setEvent(updated);
  }

  function toggle() {
    setShowEdit(!showEdit);
  }

  function subscribe() {
    if (token) {
      addSubscription(Number(id));
      setIsSubscribe(true);
    } else {
      window.location.href = "/login";
    }
  }

  function unsubscribe() {
    if (token) {
      deleteSubscription(Number(id));
      setIsSubscribe(false);
    } else {
      window.location.href = "/login";
    }
  }

  async function isSub() {
    try {
      if (await isSubscribed(Number(id))) {
        setIsSubscribe(true);
      }
    } catch {
      setIsSubscribe(false);
    }
  }

  if (!event) {
    return <p>Loading...</p>;
  }

  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  console.log(isSubscribe);

  return (
    <div>
      <div className="text-center">
        <h1 className="mb-2 pb-0">{event?.title}</h1>
        <div className="containerArticle row d-flex justify-content-center">
          <p className="mb-4">
            {event?.adresse} -{" "}
            {event?.date && new Date(event?.date).toLocaleDateString()}
          </p>
          <img
            className="card-img-top"
            key={event.id}
            src={makeLink(String(event.image))}
            alt="image de l'article"
            style={{
              height: 50 + "vw",
              objectFit: "cover",
              objectPosition: "center",
            }}
          />
          <p className="mt-4 mb-4"> {event?.content} </p>

          <div>
            {!isSubscribe ? (
              <button
                type="button"
                className="btn btn-bleu"
                onClick={subscribe}
              >
                {" "}
                S'inscrire{" "}
              </button>
            ) : (
              <button
                type="button"
                className="btn btn-bleu"
                onClick={unsubscribe}
              >
                {" "}
                Se désinscrire{" "}
              </button>
            )}
          </div>

          {showEdit && (
            <>
              <h2 style= {{marginTop:"60px", fontFamily:"Bentham"}}>Modifier l'évènement</h2>
              <FormEvent edited={event} onSubmit={update} />
            </>
          )}

          {isAdmin() && (
             <div className="justify-content-between" style= {{display:"flex", marginBottom: "40px"}}>
              <button
                className="btn btn-bleu btn-sm pe-3 ps-3 mt-4 me-2 mb-4"
                onClick={toggle}
              >
                Modifier l'événement
              </button>
              <button
                className="btn btn-bleu btn-sm pe-3 ps-3 mt-4 ms-2 mb-4"
                onClick={remove}
              >
                Supprimer l'événément
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
